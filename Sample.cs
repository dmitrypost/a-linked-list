// Also for an Ordered Linked List example that works you can find on my GitHub => https://github.com/dmitrypost/College-Assignments/tree/master/CS358%20V2%20Projects/CS358%20Projects/Assignment%203
// That is a direct link to the exact assignment which does order the list as well. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        
    }

    class Node
    {
        public Node Next;
        public string Name;
        public string Description;
        private bool Equals(Node comparedTo)
        {
            return (this.Name == comparedTo.Name || this.Description == comparedTo.Description);
        }
    }

    class LinkedList
    {
        private Node First;
        public void Add(string name, string description)
        {
            var node = new Node() { Name = name, Description = description };
            Add(node);
        }

        public void Add(Node node)
        {
            if (First == null)
                First = node;
            else
            {
                var eval = First;
                while (eval.Next != null)
                {
                    eval = eval.Next; // check the next one
                }
                // add the node
                eval.Next = node;
            }
        }

        public void Insert(int index, string name, string description)
        {
            var node = new Node() { Name = name, Description = description };
            Insert(index, node);
        }

        public void Insert(int index, Node node)
        {
            var i = 0;
            var eval = First;
            if (index == 0) //inserting as the first Node in list
                First = node; First.Next = eval;
            while (i != index) // itterate till we find the indexed node
            {
                if (eval == null)
                    throw new Exception($"Cannot insert node at {index} position");
                else
                {
                    eval = eval.Next;
                    i++;
                }
            }
            // now insert the node
            var temp = eval.Next;
            eval.Next = node;
            node.Next = temp;

        }

        public void Update(string name, string newDescription)
        {
            var eval = First;
            while (eval.Name != name)
            {
                eval = eval.Next;
                if (eval.Next == null)
                    throw new Exception($"No node with the name {name} found");
            }
            eval.Description = newDescription;
        }
        
    }
}
