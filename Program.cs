﻿using LinkedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            Lista lista = new Lista();
            Nodo nodo;
            short opcion;
            string codigo;

            Console.Title = "Lista enlazada";
            do
            {
                Console.Clear();
                Console.WriteLine("\nVagones del Tren");
                Console.WriteLine("\n 1. Agregar Vagón");
                Console.WriteLine("\n 2. Agregar vagón despues de otro especificado");
                Console.WriteLine("\n 3. Actualizar descripción del vagón ");
                Console.WriteLine("\n 4. Mostrar todos los vagones");
                Console.WriteLine("\n 5. Buscar vagón");
                Console.WriteLine("\n 6. Quitar vagón");
                Console.WriteLine("\n 7.Salir");
                Console.Write("\n Opción: ");
                opcion = short.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("\nAgregar vagones la tren");
                        Console.Write("\nIngrese código del vagón: ");
                        codigo = Console.ReadLine();

                        if (lista.Buscar(codigo) == null)
                        {
                            Console.WriteLine("\n Descripción: ");
                            string desc = Console.ReadLine();
                            nodo = new Nodo(codigo, desc);
                            lista.Insertar(nodo);
                            Console.WriteLine("Vagón {0} agregado...", codigo);
                        }
                        else                        
                            Console.WriteLine("\n El vagón ya existe");                        
                        Console.ReadLine();
                        break;
                    case 2:
                        Console.WriteLine("\nAgregar vagón despues de otro especificado");
                        Console.WriteLine("\n\tAgregar vagón despues de: ");
                        

                        break;
                    case 3:
                        Console.WriteLine("\nActualizar descripción del vagón");
                        Console.Write("\nIngrese código del vagón a actualizar: ");
                        codigo = Console.ReadLine();
                        nodo = lista.Buscar(codigo);
                        //if (nodo != null)
                        //{
                        //    Console.Write("\nIngrese nueva descripcion: ");
                        //    string desc = Console.ReadLine();
                        //    lista.actualizarDescripcion(desc);
                        //    Console.WriteLine("Vagón {0} acualizado...", codigo);
                        //}
                        //else
                        //    Console.WriteLine("\n\tEl vagón {0} no existe", codigo);
                        Console.ReadKey();
                        break;
                    case 4:
                        Console.WriteLine("\n\t Lista de vagones");
                        lista.MostrarTodos();
                        Console.ReadKey();
                        break;
                    case 5:
                        Console.WriteLine("\n\tBuscar vagon");
                        Console.Write("\nIngrese código del vagón: ");
                        codigo = Console.ReadLine();
                        nodo = lista.Buscar(codigo);
                        if (nodo != null)                      
                            lista.mostrarUno(nodo);                        
                        else                        
                            Console.WriteLine("\n\tEl vagón {0} no existe", codigo);                        
                        Console.ReadKey();
                        break;
                    case 6:
                        Console.WriteLine("\n\tQuitar vagón");
                        Console.Write("Ingrese código del vagón: ");
                        codigo = Console.ReadLine();
                        nodo = lista.Buscar(codigo);
                        if(nodo != null)                   
                            lista.eliminar(codigo);                       
                        else                        
                            Console.WriteLine("\n\tEl vagón {0} no existe", codigo);                        
                    break;
                    default:
                        break;
                }
            } while (opcion != 5);
        }
    }
}
