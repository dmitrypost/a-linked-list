﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Nodo
    {
        string codigo;
        string descripcion;
        public Nodo siguiente;

        public Nodo()
        {
            codigo = null;
            descripcion = null;
        }

        public Nodo(string cod, string desc)
        {
            this.codigo = cod;
            this.descripcion = desc;
            siguiente = null;
        }

        public string Codigo
        {
            get { return codigo; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
    }
}
