﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    class Lista
    {
        string codigo;
        Nodo primero, ultimo;
        public Lista siguiente;

        public Lista()
        {
            primero = ultimo = null;
            siguiente = null;
        }

        public string Codigo
        {
            get { return codigo; }
        }

        public void Insertar(Nodo nuevo)
        {
            if (primero == null)
                primero = nuevo;
            else
                ultimo.siguiente=nuevo;
            ultimo = nuevo;
        }

        public void insertarDespuesDe()
        {
                        
        }
    
        public void actualizarDescripcion(string desc)
        {
            
        }

        public Nodo Buscar(string cod)
        {
            Nodo actual = new Nodo();
            actual = primero;
            while(actual!=null)
            {
                if(actual.Codigo==cod)
                {
                    return actual;
                }
                actual = actual.siguiente;
            }
            return null;
        }
        public void MostrarTodos()
        {
            if(primero!=null)
            {
                Nodo actual = primero;
                Console.WriteLine("\n\t Código \tDescripción");
                while(actual!=null)
                {
                    Console.Write("\n\t {0} \t\t {1}", actual.Codigo, actual.Descripcion);
                    actual = actual.siguiente;
                }
            }
            else
            {
                Console.WriteLine("\n\t No hay vagones");
            }
        }

        public void mostrarUno(Nodo buscado)
        {
            Console.WriteLine("\n Código     : {0}", buscado.Codigo);
            Console.WriteLine("\n Descripción: {0}", buscado.Descripcion);
        }

        private Nodo buscarPadre(string cod)
        {
            Nodo padre, actual;
            actual = primero;
            padre = null;
            while(actual != null)
            {
                if(actual.Codigo == cod)
                {
                    break;
                }
                padre = actual;
                actual = actual.siguiente;
            }
            return padre;
        }

        public void eliminar(string cod)
        {
            Nodo actual, padre;
            padre = buscarPadre(cod);
            if(padre == null)
            {
                actual = primero;
                primero = primero.siguiente;
                padre = primero;
            }
            else
            {
                actual = padre.siguiente;
                padre.siguiente = actual.siguiente;
            }
            actual = null;
            if(padre == null || padre.siguiente == null)
                ultimo = padre;
            Console.WriteLine("\n\tVagón {0} quitado del tren", cod);
        }

        
    }
}
